(function($) {

	$(document).ready(function() {

	"use strict";

		// Init Masonry

		var $grid = $('.masonry-grid').masonry({
			itemSelector: '.masonry-grid__item',
			percentPosition: true,
			gutter: 5
		});

		// Layout Masonry after each image loads

		$grid.imagesLoaded().progress(function () {
			$grid.masonry('layout');
		});

	});

})(jQuery);